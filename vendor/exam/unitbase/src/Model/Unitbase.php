<?php
/* Class: Unitbase
 * exam
 * 
 * Model for Unitbase
 * FileName: exam/Unitbase/src/Model/Unitbase.php
*/
namespace Exam\Unitbase\Model;
class Unitbase extends \ModernWays\Mvc\Model
{
    private $id;
    private $name;
    private $description;
    private $code;
    
    
    public function getId()
    {
        return $this->id;
    }

    
    public function setId($id)
    {
        $this->id = $id;
    }   
    
    
    public function getName()
    {
        return $this->name;
    }
    
   
    public function getNameUpperCase()
    {
        return strtoupper($this->name);
    }

    
    public function setName($name)
    {
        // alleen letters en cijfers en èéë en niet leeg
        if (strlen($name) == 0) {
            $this->modelState->startTimeInKey('Unitbase Name');
            $this->modelState->setText('Naam kan niet leeg zijn!');
            $this->modelState->setCaption('Unitbase Name insert');
            $this->modelState->setCode('007');
            $this->modelState->log();
        } else {
            if (preg_match("/^[a-zA-Z ]*$/", $name)) {
                 $this->name = $name;
            } else {
                $this->modelState->startTimeInKey('Unitbase Name');
                $this->modelState->setText('Alleen letters en cijfers!');
                $this->modelState->setCaption('Unitbase Name insert');
                $this->modelState->setCode('007');
                $this->modelState->log();
            }
        }
    }  
    
    
    public function getDescription()
    {
        return $this->description;
    }

    
    public function setDescription($description)
    {
        $this->description = $description;
    }  
    
    
    public function getCode()
    {
        return $this->code;
    }

    
    public function setCode($code)
    {
        $this->code = $code;
    }  
    
    
}