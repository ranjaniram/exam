<main>
    <header>
        <h1> Editing Page</h1>
    </header>
    <article>
        <nav>
          <h3><a href="/unitbase.php/Home/Inserting">Inserting</a></h3>  
            <a href=""></a>
        </nav>
        <section>
            <?php
                if (count($model->getList()) > 0) { ?>
                <table class="table table-striped">
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Code</th>
                        <th></th>
                    </tr>
                    <?php foreach($model->getList() as $item) { ?>
                    <tr>
                        <td>
                            <?php echo $item['Id'];?>
                            <!--[Id] is from PDO-->
                        </td>
                        <td>
                            <?php echo $item['Name'];?>
                        </td>
                        <td>
                            <?php echo $item['Description'];?>
                        </td>
                        <td>
                            <?php echo $item['Code'];?>
                        </td>
                        <td><a href="/unitbase.php/Home/ReadingOne/<?php echo $item['Id'];?>">Select</a></td>
                        
                    </tr>
                    <?php
                    }
                    ?>
                </table>
                <?php
                } else { ?>
                    <p>Found no Unitbase</p>
                <?php 
                } ?>
        </section>
    </article>
    <footer></footer>
</main>
</div>
<!-- deze methode toont het prikbord van de controller -->
<?php $appStateView(); ?>
<!-- er bestaat geen methode in 3p MVC om het prikbord van
     het model te tonen -->
   
<pre><?php print_r($model->getModelState()->getBoard()); ?></pre>

