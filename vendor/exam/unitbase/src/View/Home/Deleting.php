<!-- Use case for deleting -->

<main>
    <header>
        <h3>Delete</h3>
    </header>
    <article>
        <nav>
            <a href=""></a>
            <a href=""></a>
        </nav>
        <h2>Are you sure you want to delete this?</h2>
        <form action="/unitbase.php/Home/Delete" method="post">
            <!-- (div>label+input)*3 -->
            <div class="form-group">
            <label for="UnitbaseId">Id : <?php echo $model->getId();?> </label>
            </div>
            <div class="form-group">
                <label for="UnitbaseName">Name : <?php echo $model->getName();?></label>
            </div>
            <div class="form-group">
                <label for="UnitbaseDescription">Description : <?php echo $model->getDescription();?></label>
            </div>
            <div class="form-group">
                <label for="UnitbaseCode">Code : <?php echo $model->getCode();?></label>
            </div>
            <button type="submit">Delete</button>
        </form>
    </article>
</main>
</div>
 <?php $appStateView(); ?>
</body>
</html>
