<?php

$UnitbaseNameError = $UnitbaseDescriptionError = $UnitbaseCodeError = '';

if(isset($model) && $model->getModelState() != null) {
    foreach ($model->getModelState()->getBoard() as $value) {
        if($value->getCaption() == 'UnitbaseName')
            $UnitbaseNameError = $value->getText();
            
        if($value->getCaption() == 'UnitbaseDescription')
            $UnitbaseDescriptionError = $value->getText();
            
        if($value->getCaption() == 'UnitbaseCode')
            $UnitbaseCodeError = $value->getText();
    }
}
?>
<main>
    <header>
        <h1>Inserting an Unitbase</h1>
    </header>
    <article>
        <nav>
            <a href=""></a>
            <a href=""></a>
        </nav>
       <form method="POST" action="/unitbase.php/home/insert">
        <div class="form-group">
        <label for="UnitbaseName">Name:</label><br/>
        <input type="text" name="UnitbaseName" value="<?php echo isset($model) && !empty($model->getName()) ? $model->getName() : '' ?>"/>
         <span><?php echo $UnitbaseNameError; ?></span>
        </div>
        <div class="form-group">
        <label for="UnitbaseDescription">Description:</label><br/>
        <input type="text" name="UnitbaseDescription" value="<?php echo isset($model) && !empty($model->getDescription()) ? $model->getDescription() : '' ?>"/>
        <span><?php echo $UnitbaseDescriptionError; ?></span>
        </div>
        <div class="form-group">
        <label for="UnitbaseCode">Code:</label><br/>
        <input type="text" name="UnitbaseCode" value="<?php echo isset($model) && !empty($model->getCode()) ? $model->getCode() : '' ?>"/>
        <span><?php echo $UnitbaseCodeError; ?></span>
        </div>
        <input type="submit" value="Insert" />
        </form>
    </article>
    <footer></footer>
</main>
</div>
<?php $appStateView(); ?>

<pre><?php print_r($model->getModelState()->getBoard()); ?></pre>