<?php
namespace Exam\Unitbase\Controller;

class Home extends \ModernWays\Mvc\Controller
{
    
    private $pdo;
    private $model;
    
    public function __construct(\ModernWays\Mvc\Route $route = null, \ModernWays\Dialog\Model\INoticeBoard $noticeBoard = null) {
        
        parent::__construct($route, $noticeBoard);
        $this->noticeBoard->startTimeInKey('PDO Connection ');
        try {
        $this->pdo = new \PDO('mysql:host=localhost;dbname=ranjaniramanuja;charset=utf8', 'ranjaniramanuja', '');
            $this->noticeBoard->setText('PDO connection connected!');
            $this->noticeBoard->setCaption('PDO connection for Unitbase');
        } catch (\Exception $e) {
            $this->noticeBoard->setText("{$e->getMessage()} on line {$e->getLine()} in file {$e->getFile()}");
            $this->noticeBoard->setCaption('PDO connection for Unitbase');
            $this->noticeBoard->setCode($e->getCode());
        }
       $this->noticeBoard->log(); 
        // create een instantie van een prikbord om validatiefouten
        // op te plaatsen
        $modelState = new \ModernWays\Dialog\Model\NoticeBoard();
        $this->model = new \Exam\Unitbase\Model\Unitbase($modelState);
    } 
         

    public function editing()
    {
       if ($this->pdo) {
            // het model vullen
            $command = $this->pdo->query("call UnitbaseSelectAll");
             //Associatieve Array Columns and value per row
            $this->model->setList($command->fetchAll(\PDO::FETCH_ASSOC));
            //returning to View
            return $this->view('Home','Editing', $this->model);
        }
        
        else {
            return $this->view('Home', 'Error', null);
         }
    }
    
    public function readingone()
    {
        if ($this->pdo) {
            
            $this->model->setId($this->route->getId());
            $statement = $this->pdo->prepare("call UnitbaseSelectOne(:pId)"); 
            $statement->bindValue(':pId', $this->model->getId(), \PDO::PARAM_INT);
            $result = $statement->execute();
            $unitbaseOne = $statement->fetch(\PDO::FETCH_ASSOC);
            $this->model->setName($unitbaseOne['Name']);
            $this->model->setDescription($unitbaseOne['Description']);
            $this->model->setCode($unitbaseOne['Code']);
            return $this->view('Home','ReadingOne', $this->model);
        } else {
            return $this->view('Home', 'Error', null);
        }
        
    }
    
    public function inserting()
    {
        return $this-> view('Home', 'Inserting', null);
    }
    
    public function insert()
    {
        if (!$this->pdo) {
             return $this->view('Home', 'Error', null);
        }
        if ($this->model->isValid()) {
    
        $this->model->setName(filter_input(INPUT_POST, 'UnitbaseName', FILTER_SANITIZE_STRING));
        $this->model->setDescription(filter_input(INPUT_POST, 'UnitbaseDescription', FILTER_SANITIZE_STRING));
        $this->model->setCode(filter_input(INPUT_POST, 'UnitbaseCode', FILTER_SANITIZE_STRING));
            $statement = $this->pdo->prepare("call UnitbaseInsert(:pName, 
                :pDescription , :pCode, @pId)");
            $statement->bindValue(':pName', $this->model->getName(), \PDO::PARAM_STR);
            $statement->bindValue(':pDescription', $this->model->getDescription(), \PDO::PARAM_STR);
            $statement->bindValue(':pCode', $this->model->getCode(), \PDO::PARAM_STR);
            $result = $statement->execute();
            return $this->editing();
        } else {
            return $this->view('Home', 'Inserting', null);
        }
    }
    
    public function updating() {
        if ($this->pdo) {
            $this->model->setId($this->route->getId());
            $statement = $this->pdo->prepare("call UnitbaseSelectOne(:pId)"); 
            $statement->bindValue(':pId', $this->model->getId(), \PDO::PARAM_INT);
            $result = $statement->execute();
            $unitbaseOne = $statement->fetch(\PDO::FETCH_ASSOC);
            $this->model->setName($unitbaseOne['Name']);
            $this->model->setDescription($unitbaseOne['Description']);
            $this->model->setCode($unitbaseOne['Code']);
            return $this->view('Home','Updating', $this->model);
        } else {
            return $this->view('Home', 'Error', null);
        }
        
    }
    
    public function update() {
        if (!$this->pdo) {
             return $this->view('Home', 'Error', null);
        }
        
        $this->model->setName(filter_input(INPUT_POST, 'UnitbaseName', FILTER_SANITIZE_STRING));
        $this->model->setDescription(filter_input(INPUT_POST, 'UnitbaseDescription', FILTER_SANITIZE_STRING));
        $this->model->setCode(filter_input(INPUT_POST, 'UnitbaseCode', FILTER_SANITIZE_STRING));
        $this->model->setId(filter_input(INPUT_POST, 'UnitbaseId', FILTER_SANITIZE_NUMBER_INT));
        
        
        if ($this->model->isValid()) {
            $statement = $this->pdo->prepare("call UnitbaseUpdate(:pName,:pDescription, :pCode, :pId)");
            
            $statement->bindValue(':pName', $this->model->getName(), \PDO::PARAM_STR);
            $statement->bindValue(':pDescription', $this->model->getDescription(), \PDO::PARAM_STR);
            $statement->bindValue(':pCode', $this->model->getCode(), \PDO::PARAM_STR);
            $statement->bindValue(':pId', $this->model->getId(), \PDO::PARAM_INT);
            $result = $statement->execute();
            return $this->editing();
        } else {
             return $this->view('Home', 'Updating', null);
        }
     }
     
    public function deleting()
    {
        if ($this->pdo) {
            
            $this->model->setId($this->route->getId());
            $statement = $this->pdo->prepare("call UnitbaseSelectOne(:pId)"); 
            $statement->bindValue(':pId', $this->model->getId(), \PDO::PARAM_INT);
            $result = $statement->execute();
            $unitbaseOne = $statement->fetch(\PDO::FETCH_ASSOC);
            $this->model->setName($unitbaseOne['Name']);
            $this->model->setDescription($unitbaseOne['Description']);
            $this->model->setCode($unitbaseOne['Code']);
            return $this->view('Home','Deleting', $this->model);
        } else {
            return $this->view('Home', 'Error', null);
        }
    }
    
    public function delete() {
        if($this->pdo) {
            $statement = $this->pdo->prepare("call UnitbaseDelete(:pId)");
            $statement->bindValue(':pId', $this->route->getId(), \PDO::PARAM_INT);
            $statement->execute();
            
            return $this->editing();
        } else {
            return $this->view('Home', 'Error', null);
        }
        
    }
}