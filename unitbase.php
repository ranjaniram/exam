<?php
    include ( __DIR__ . '/vendor/autoload.php');
    
    $appState = new \ModernWays\Dialog\Model\NoticeBoard();
    $request = new \ModernWays\Mvc\Request('/home/editing');
    $route = new \ModernWays\Mvc\Route($appState, $request->uc());
    
    /* NOTE : the namespace in which classes of my app/project
    in psr4 autoLoad have to specify the path where the classes*/
      
    /* The following method creates an instance of the class Home and executes the method
      Index of that class, under the condition that there is no other route is passed.*/
    $routeConfig = new \ModernWays\Mvc\RouteConfig('\Exam\Unitbase', $route, $appState);
    $view = $routeConfig->invokeActionMethod();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Article Page</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <?php $view();?>
</div>
</body>
</html>

